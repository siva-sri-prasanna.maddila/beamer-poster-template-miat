# Poster Template for LaTeX

This is my personal template for posters I make while part of MIAT. It's on the Forge because a) I can create copies easily from anywhere and b) track its evolution. If useful, please use it :)
